<?php

namespace App\Http\Controllers;

use App\Lead;
use Schema;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;
use Kris\LaravelFormBuilder\Field;
use Kris\LaravelFormBuilder\Form;
use Symfony\Component\VarDumper\VarDumper;


class LeadController extends Controller
{

    private $formBuilder;

    function __construct(FormBuilder $formBuilder)
    {
        $this->formBuilder = $formBuilder;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $content = '';

        if(!empty(implode($request->except('_token')))){
            $input = $request->all();
            $input = $request->except('_token');
            $leadSearch = Lead::query();

            foreach ($input as $key => $value) {
                if(!is_null($value)) {
                    $leadSearch->where($key, $value);
                }
            }

            $content = $leadSearch->get();
        }
        

        
        return view('layouts.tables',
            [
                'headers' => Schema::getColumnListing('leads'),
                'info' => !empty($content) ? $content : ''
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Formbuilder $formBuilder)
    {

        $test = Lead::get('id');
        

        #dd(array_keys($test->first()->getAttributes()));

        $form = $this->buildForm();

        return view('layouts.test',compact('form'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $form = $this->buildForm();

        #$form->redirectIfNotValid();
        if(!$form->isValid()){
            redirect()->back();
        }
        #$form->getFieldValues();
        return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function show(Lead $lead)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function edit(Lead $lead)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lead $lead)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lead $lead)
    {
        //
    }

    private function buildForm()
    {
        $number = '';

        return $this->formBuilder->createByArray([
            // [
            //     'name' => 'name',
            //     'type' => Field::TEXTAREA,
            //     'choices' => '',
            //     'rules' => 'required|regex:/^.+$/i',
            //     'wrapper' => [
            //         'class' => 'col-md-' . $number
            //     ],
            //      'attr' => [ 'pattern' => ''],
                
            // ],
            [
                'name' => 'id',
                'label' => 'ID',
                'type' => Field::TEXT,
                'attr' => [
                    'id' => 'id',
                    'class' => 'form-control',
                    'placeholder' => 'Id',                    
                ],
                'wrapper' => [
                    'class' => 'form-group col-md-4'
                ]
            ],
            [
                'name' => 'guid',
                'label' => 'Guid',
                'type' => Field::TEXT,
                'attr' => [
                    'id' => 'guid',
                    'class' => 'form-control',
                    'placeholder' => 'Guid',
                ],
                'wrapper' => [
                    'class' => 'form-group col-md-4'
                ]
            ],
            [
                'name' => 'phoneNumber',
                'label' => 'phoneNumber',
                'type' => Field::TEXT,
                'attr' => [
                    'id' => 'phoneNumber',
                    'class' => 'form-control',
                    'placeholder' => 'Phone Number',
                ],
                'wrapper' => [
                    'class' => 'form-group col-md-4'
                ]
            ],
            [
                'name' => 'email',
                'label' => 'Email',
                'type' => Field::TEXT,
                'attr' => [
                    'id' => 'email',
                    'class' => 'form-control',
                    'placeholder' => 'Email',
                ],
                'wrapper' => [
                    'class' => 'form-group col-md-4'
                ]
            ],
            [
                'name' => 'fullName',
                'label' => 'Full Name',
                'type' => Field::TEXT,
                'attr' => [
                    'id' => 'fullName',
                    'class' => 'form-control',
                    'placeholder' => 'Full Name',
                ],
                'wrapper' => [
                    'class' => 'form-group col-md-4'
                ]
            ],
            [
                'name' => 'gender',
                'label' => 'Gender',
                'type' => Field::SELECT,
                'attr' => [
                    'id' => 'gender',
                    'class' => 'form-control',
                    'placeholder' => 'Gender',
                ],
                'wrapper' => [
                    'class' => 'form-group col-md-4'
                ],
                'choices' => [
                    'm' => 'Male',
                    'f' => 'Female'
                ],
                'empty_value' => '--'
            ],
            
        ]
        ,[
        'method' => 'POST',
        'url' => route('leads.store'),
        'class' => 'row'
        
        ]);
    }
}
