<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Lead;
use Faker\Generator as Faker;

$factory->define(Lead::class, function (Faker $faker) {
    return [
        'guid' => $faker->uuid,
        'phonenumber' => $faker->phoneNumber,
        'email' => $faker->unique()->safeEmail,
        'fullName' => $faker->name,
        'gender' => $faker->randomElement(['M','F']),
        'birthDate' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'ip' => $faker->ipv4,
        
    ];
});
