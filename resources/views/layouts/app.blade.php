<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    
    

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600,700,800" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/iconkit.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/perfect-scrollbar.css') }}" rel="stylesheet">
    <link href="{{ asset('css/dataTables.bootstrap4.min.css') }}"  rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('datatables.min.css') }}"/>
    
</head>
<body>
    <div id="app" class="wrapper">
        @include('partials.header')
        <div class="page-wrap">

            @include('partials.sidebar')

            <div class="main-content">

                <div class="container-fluid">
                

                    @yield('content')
                    

                </div> 

            </div>
        </div>
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
    <!--<script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>-->
    <script src="{{ asset('js/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{ asset('js/theme.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/datatables.min.js')}}"></script>
    <script src="{{ asset('js/dataTables.js')}}"></script>
    
    
</body>
</html>
