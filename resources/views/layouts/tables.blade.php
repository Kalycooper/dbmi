@extends('layouts.app')

@section('content')

<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="ik ik-edit bg-blue"></i>
                <div class="d-inline">
                    <h5>Leads</h5>
                    <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <nav class="breadcrumb-container" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="home"><i class="ik ik-home"></i></a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Sales</li>
                    <li class="breadcrumb-item active" aria-current="page">Leads</li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header"><h3>Search Filters</h3></div>
            <div class="card-body">

                <form class="row" method="GET">

                    @csrf 

                    <div class="form-group col-md-4">
                        <label for="id">ID</label>
                        <input type="text" name="id" class="form-control" id="id" placeholder="Id">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="guid">Guid</label>
                        <input type="text" class="form-control" id="guid" name="guid" placeholder="Guid">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="phoneNumber">PhoneNumber</label>
                        <input type="text" class="form-control" id="phoneNumber" name="phoneNumber" placeholder="Phone Number">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="exampleInputConfirmPassword1">Email</label>
                        <input type="text" class="form-control" id="email" name="email" placeholder="email">
                    </div>

                    <div class="form-group col-md-4">
                        <label for="fullName">Full Name</label>
                        <input type="text" class="form-control" id="fullName" name="fullName" placeholder="Full Name">
                    </div>

                    <div class="form-group col-md-4">
                        <label for="gender">Gender</label>
                        <select class="form-control" id="gender" name="gender">
                            <option value="">--</option>
                            <option value="M">Male</option>
                            <option value="F">Female</option>
                        </select>
                    </div>

                    <div class="form-group col-12">
                        <button type="submit" class="btn btn-primary mr-2">Submit</button>
                        <button class="btn btn-light">Cancel</button>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header"><h3>Search Results</h3></div>
            <div class="card-body">

                <table id="data_table" class="table responsive no-wrap" width="100%">
                    <thead>
                        <tr>
                            @foreach ($headers as $header)
                                
                                <th>{{ ucfirst($header) }}</th>                       

                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @if (!empty($info))
                            @foreach ($info as $lead)
                                <tr>
                                    @foreach ($headers as $header)
                                        <td>{{ $lead[$header] }}</td>
                                    @endforeach
                                </tr>   
                            @endforeach
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            @foreach ($headers as $header)
                                
                                <th>{{ ucfirst($header) }}</th>                       

                            @endforeach
                        </tr>
                        </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>



@endsection
