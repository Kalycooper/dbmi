
<div class="app-sidebar colored">
    <div class="sidebar-header">
        <a class="header-brand" href="index.html">
            <div class="logo-img">
                
            </div>
            <span class="text">DBMI</span>
        </a>
        <button type="button" class="nav-toggle"><i data-toggle="expanded" class="ik ik-toggle-right toggle-icon"></i></button>
        <button id="sidebarClose" class="nav-close"><i class="ik ik-x"></i></button>
    </div>
    
    <div class="sidebar-content">
        <div class="nav-container">

            <nav id="main-menu-navigation" class="navigation-main">

                <div class="nav-item  {{ request()->routeIs('home') ? 'active' : '' }}">
                    <a href="{{ route('home') }}"><i class="ik ik-home"></i><span>Dashboard</span></a>
                </div>

                <div class="nav-lavel">Sales</div>

                <div class="nav-item {{ request()->routeIs('leads') ? 'active' : '' }}">
                    <a href="{{ route('leads') }}"><i class="ik ik-database"></i><span>Leads</span></a>
                </div>

                <div class="nav-item {{ request()->routeIs('leads.create') ? 'active' : '' }}">
                    <a href="{{ route('leads.create') }}"><i class="ik ik-file-minus"></i><span>Deals</span></a>
                </div>

                <div class="nav-item">
                    <a href="clients"><i class="ik ik-users"></i><span>Clients</span> <span class="badge badge-success">New</span></a>
                </div>

                <div class="nav-item has-sub">
                    <a href="javascript:void(0)"><i class="ik ik-users"></i><span>Widgets</span> <span class="badge badge-danger">150+</span></a>
                    <div class="submenu-content">
                        <a href="pages/widgets.html" class="menu-item">Basic</a>
                        <a href="pages/widget-statistic.html" class="menu-item">Statistic</a>
                        <a href="pages/widget-data.html" class="menu-item">Data</a>
                        <a href="pages/widget-chart.html" class="menu-item">Chart Widget</a>
                    </div>
                </div>             

                <div class="nav-lavel">WorkFlow</div>

                <div class="nav-item has-sub">
                    <a href="javascript:void(0)"><i class="ik ik-git-branch"></i><span>Tasks</span></a>
                    <div class="submenu-content">
                        <a href="tasks/my" class="menu-item">My Tasks</a>
                        <a href="tasks/create" class="menu-item">Create Task</a>
                        <a href="tasks/all" class="menu-item">All Tasks</a>
                    </div>
                </div>   

                <div class="nav-lavel">Support</div>

                <div class="nav-item">
                    <a href="javascript:void(0)"><i class="ik ik-monitor"></i><span>Documentation</span></a>
                </div>

                <div class="nav-item">
                    <a href="javascript:void(0)"><i class="ik ik-help-circle"></i><span>Submit Issue</span></a>
                </div>

            </nav>
        </div>
    </div>
</div>
